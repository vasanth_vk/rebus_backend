const busAmenitiesData = [
    {
        id: 1,
        busID: 1,
        amenitiesID: 1,
    },
    {
        id: 2,
        busID: 1,
        amenitiesID: 3,
    },
    {
        id: 3,
        busID: 1,
        amenitiesID: 5,
    },
    {
        id: 4,
        busID: 1,
        amenitiesID: 7,
    },
    {
        id: 5,
        busID: 1,
        amenitiesID: 9,
    },
    {
        id: 6,
        busID: 2,
        amenitiesID: 2,
    },
    {
        id: 7,
        busID: 2,
        amenitiesID: 4,
    },
    {
        id: 8,
        busID: 2,
        amenitiesID: 6,
    },
    {
        id: 9,
        busID: 3,
        amenitiesID: 1,
    },
    {
        id: 10,
        busID: 3,
        amenitiesID: 4,
    },
    {
        id: 11,
        busID: 3,
        amenitiesID: 5,
    },
    {
        id: 12,
        busID: 4,
        amenitiesID: 6,
    },
    {
        id: 13,
        busID: 4,
        amenitiesID: 2,
    },
    {
        id: 14,
        busID: 4,
        amenitiesID: 4,
    },
    {
        id: 15,
        busID: 5,
        amenitiesID: 5,
    },
    {
        id: 16,
        busID: 5,
        amenitiesID: 1,
    },
    {
        id: 17,
        busID: 5,
        amenitiesID: 2,
    },
    {
        id: 18,
        busID: 6,
        amenitiesID: 3,
    },
    {
        id: 19,
        busID: 6,
        amenitiesID: 4,
    },
    {
        id: 20,
        busID: 6,
        amenitiesID: 6,
    },
    {
        id: 21,
        busID: 7,
        amenitiesID: 2,
    },
    {
        id: 22,
        busID: 7,
        amenitiesID: 4,
    },
    {
        id: 23,
        busID: 7,
        amenitiesID: 5,
    },
    {
        id: 24,
        busID: 8,
        amenitiesID: 1,
    },
    {
        id: 25,
        busID: 8,
        amenitiesID: 4,
    },
    {
        id: 26,
        busID: 9,
        amenitiesID: 5,
    },
    {
        id: 27,
        busID: 9,
        amenitiesID: 6,
    },
    {
        id: 28,
        busID: 9,
        amenitiesID: 2,
    },
    {
        id: 29,
        busID: 9,
        amenitiesID: 4,
    },
    {
        id: 30,
        busID: 10,
        amenitiesID: 5,
    },
    {
        id: 31,
        busID: 10,
        amenitiesID: 1,
    },
    {
        id: 32,
        busID: 11,
        amenitiesID: 2,
    },
    {
        id: 33,
        busID: 11,
        amenitiesID: 3,
    },
    {
        id: 34,
        busID: 11,
        amenitiesID: 4,
    },
    {
        id: 35,
        busID: 11,
        amenitiesID: 6,
    },
    {
        id: 36,
        busID: 12,
        amenitiesID: 2,
    },
    {
        id: 37,
        busID: 12,
        amenitiesID: 4,
    },
    {
        id: 38,
        busID: 12,
        amenitiesID: 5,
    },
    {
        id: 39,
        busID: 13,
        amenitiesID: 1,
    },
    {
        id: 40,
        busID: 14,
        amenitiesID: 4,
    },
    {
        id: 41,
        busID: 15,
        amenitiesID: 5,
    },
    {
        id: 42,
        busID: 15,
        amenitiesID: 6,
    },
    {
        id: 43,
        busID: 15,
        amenitiesID: 2,
    },
    {
        id: 44,
        busID: 16,
        amenitiesID: 4,
    },
    {
        id: 45,
        busID: 17,
        amenitiesID: 5,
    },
    {
        id: 46,
        busID: 18,
        amenitiesID: 1,
    },
    {
        id: 47,
        busID: 18,
        amenitiesID: 2,
    },
    {
        id: 48,
        busID: 18,
        amenitiesID: 3,
    },
    {
        id: 49,
        busID: 19,
        amenitiesID: 1,
    },
    {
        id: 50,
        busID: 19,
        amenitiesID: 6,
    },
    {
        id: 51,
        busID: 19,
        amenitiesID: 2,
    },
    {
        id: 52,
        busID: 19,
        amenitiesID: 4,
    },
    {
        id: 53,
        busID: 20,
        amenitiesID: 5,
    },
    {
        id: 54,
        busID: 20,
        amenitiesID: 1,
    },
    {
        id: 55,
        busID: 21,
        amenitiesID: 4,
    },
    {
        id: 56,
        busID: 21,
        amenitiesID: 5,
    },
    {
        id: 57,
        busID: 22,
        amenitiesID: 6,
    },
    {
        id: 58,
        busID: 23,
        amenitiesID: 2,
    },
    {
        id: 59,
        busID: 24,
        amenitiesID: 4,
    },
    {
        id: 60,
        busID: 24,
        amenitiesID: 5,
    },
    {
        id: 61,
        busID: 25,
        amenitiesID: 1,
    },
    {
        id: 62,
        busID: 25,
        amenitiesID: 2,
    },
    {
        id: 63,
        busID: 25,
        amenitiesID: 3,
    },
    {
        id: 64,
        busID: 25,
        amenitiesID: 4,
    },
];
module.exports = busAmenitiesData;
