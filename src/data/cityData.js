const cityData = [
    { id: 1, city: "Bengaluru" },
    { id: 2, city: "Delhi" },
    { id: 3, city: "Mumbai" },
    { id: 4, city: "Hyderabad" },
    { id: 5, city: "Chennai" },
    { id: 6, city: "Pune" },
    { id: 7, city: "Goa" },
];
module.exports = cityData;
