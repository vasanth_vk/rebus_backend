const amenitiesData = [
    { id: 1, amenity: "Staff with masks" },
    { id: 2, amenity: "Hand Sanitisers Provided" },
    { id: 3, amenity: "Deep Cleaned Buses" },
    { id: 4, amenity: "Emergency Contact Number" },
    { id: 5, amenity: "Track My Bus" },
    { id: 6, amenity: "Charging Point" },
    { id: 7, amenity: "Water Bottle" },
    { id: 8, amenity: "Reading Light" },
    { id: 9, amenity: "Wifi" },
];
module.exports = amenitiesData;
