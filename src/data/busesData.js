const busesData = [
    {
        id: 1,
        operatorID: 1,
    },
    {
        id: 2,
        operatorID: 1,
    },
    {
        id: 3,
        operatorID: 4,
    },
    {
        id: 4,
        operatorID: 5,
    },
    {
        id: 5,
        operatorID: 6,
    },
    {
        id: 6,
        operatorID: 7,
    },
    {
        id: 7,
        operatorID: 8,
    },
    {
        id: 8,
        operatorID: 1,
    },
    {
        id: 9,
        operatorID: 2,
    },
    {
        id: 10,
        operatorID: 3,
    },
    {
        id: 11,
        operatorID: 11,
    },
    {
        id: 12,
        operatorID: 1,
    },
    {
        id: 13,
        operatorID: 2,
    },
    {
        id: 14,
        operatorID: 5,
    },
    {
        id: 15,
        operatorID: 4,
    },
    {
        id: 16,
        operatorID: 9,
    },
    {
        id: 17,
        operatorID: 10,
    },
    {
        id: 18,
        operatorID: 11,
    },
    {
        id: 19,
        operatorID: 11,
    },
    {
        id: 20,
        operatorID: 11,
    },
    {
        id: 21,
        operatorID: 12,
    },
    {
        id: 22,
        operatorID: 12,
    },
    {
        id: 23,
        operatorID: 13,
    },
    {
        id: 24,
        operatorID: 14,
    },
    {
        id: 25,
        operatorID: 14,
    },
];

module.exports = busesData;
