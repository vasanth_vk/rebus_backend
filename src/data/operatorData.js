const operatorData = [
    {
        id: 1,
        operator: "SRS Travels",
    },
    {
        id: 2,
        operator: "Mahadev Travels",
    },
    {
        id: 3,
        operator: "Jakhar Travels",
    },
    {
        id: 4,
        operator: "M R Travels",
    },
    {
        id: 5,
        operator: "SAS Travels",
    },
    {
        id: 6,
        operator: "Meera Tours & Travels",
    },
    {
        id: 7,
        operator: "Khushi Tourist",
    },
    {
        id: 8,
        operator: "Jkk Travels",
    },
    {
        id: 9,
        operator: "Morning Star Travels",
    },
    {
        id: 10,
        operator: "Big Bus",
    },
    {
        id: 11,
        operator: "Varsha Travels",
    },
    {
        id: 12,
        operator: "Simhapuri Travels",
    },
    {
        id: 13,
        operator: "Intercity travels",
    },
    {
        id: 14,
        operator: "Seabird Tourists",
    },
];

module.exports = operatorData;
