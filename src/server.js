const express = require("express");
const app = express();
const cors = require("cors");
const { port } = require("./config");
const findRoute = require("./controllers/controller");

app.use(cors());
app.set("json spaces", 4);

app.use(express.json());
app.use(
    express.urlencoded({
        extended: true,
    })
);

app.get("/", (req, res) => {
    res.json({ message: "home" });
});

app.post("/busRoute", findRoute);

app.use((req, res, next) => {
    res.status(404).json({ message: "NOT FOUND" });
});

app.use((err, req, res, next) => {
    console.log(err);
    res.status(500).json({ message: "TRY AGAIN LATER" });
});

app.listen(port, () => {
    console.log(`Sever listening at ${port}`);
});
