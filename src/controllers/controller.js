const Sequelize = require("sequelize");
const Amenity = require("../models/amenity");
const Bus = require("../models/bus");
const BusAmenity = require("../models/busAmenity");
const City = require("../models/city");
const Operator = require("../models/operator");
const Route = require("../models/route");
const Seat = require("../models/seat");

const findCity = async (city) => {
    const result = await City.findAll({
        attributes: ["id", "city"],
        where: {
            city: city,
        },
        raw: true,
    });
    return result.length ? result[0] : null;
};

const findBusAmenity = async (busID) => {
    BusAmenity.belongsTo(Amenity, { foreignKey: "amenitiesID" });
    let result = await BusAmenity.findAll({
        attributes: ["amenity.amenity", "busID"],
        where: {
            busID: busID,
        },
        include: [
            {
                model: Amenity,
            },
        ],
        raw: true,
        nest: true,
    });
    result = result.reduce((a, b) => {
        if (a[b["busID"]]) {
            a[b["busID"]].push(b.amenity);
        } else {
            a[b["busID"]] = [b.amenity];
        }
        return a;
    }, {});
    return result;
};

const findSeats = async (routeID) => {
    Route.belongsTo(Seat, { foreignKey: "id" });

    let result = await Seat.findAll({
        attributes: ["id", "seat", "seatBooked", "routeID"],
        where: {
            routeID: routeID,
        },
        raw: true,
        nest: true,
    });
    result = result.reduce((acc, seat) => {
        if (acc[seat["routeID"]]) {
            acc[seat["routeID"]].push(seat);
        } else {
            acc[seat["routeID"]] = [seat];
        }
        return acc;
    }, {});
    return result;
};

const findRoute = async (req, res, next) => {
    let source = req.body.source;
    let destination = req.body.destination;
    let departureDate = req.body.departureDate;
    try {
        const cityIds = await Promise.all([
            findCity(source),
            findCity(destination),
        ]);
        const srcID = cityIds[0].id;
        const destID = cityIds[1].id;

        Route.belongsTo(Bus, { foreignKey: "busID" });
        Bus.belongsTo(Operator, { foreignKey: "operatorID" });
        Route.belongsTo(City, { foreignKey: "sourceCityID" });
        Route.belongsTo(City, { foreignKey: "destinationCityID" });

        let routesFound = await Route.findAll({
            attributes: [
                "id",
                "departureTime",
                "arrivalTime",
                "arrivalDate",
                "departureDate",
                "busID",
                "bus.operator.operator",
                "fare",
            ],
            where: {
                sourceCityID: srcID,
                destinationCityID: destID,
                departureDate: departureDate,
            },
            include: [
                {
                    model: Bus,
                    include: [
                        {
                            model: Operator,
                            required: true,
                        },
                    ],
                },
            ],
            raw: true,
            nest: true,
        });

        let routeIDArray = [];
        let busIDArray = [];
        routesFound.forEach((route) => busIDArray.push(route.busID));
        routesFound.forEach((route) => routeIDArray.push(route.id));
        const amenity = await findBusAmenity(busIDArray);
        const seat = await findSeats(routeIDArray);
        routesFound.map((route) => {
            return (route["amenity"] = amenity[route["busID"]]);
        });
        routesFound.map((route) => {
            return (route["seat"] = seat[route["id"]]);
        });
        console.log(routesFound);
        res.json(routesFound);
    } catch (error) {
        console.log(error);
        next(error);
    }
};

// findRoute("Bengaluru", "Chennai", "2021-05-29").then((data) =>
//     console.log(data)
// );

// findSeats([1]);
// Seat.update({ seatBooked: false }, { where: { id: 28 } })
//     .then((result) => console.log(result))
//     .catch((err) => console.err(err));

module.exports = findRoute;
