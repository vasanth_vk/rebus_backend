const Sequelize = require("sequelize");
const { connection } = require("./index");
const Operator = require("./operator");

const Bus = connection.define("buses", {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false,
    },
});

Operator.hasMany(Bus, { foreignKey: "operatorID" });

module.exports = Bus;
