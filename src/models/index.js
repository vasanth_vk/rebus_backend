const config = require("../config");
const Sequelize = require("sequelize");

const connection = new Sequelize(
    config.mysql_database_name,
    config.mysql_db_user,
    config.mysql_db_password,
    {
        host: config.mysql_db_host,
        dialect: config.dialect,
        pool: {
            max: config.pool.max,
            min: config.pool.min,
            acquire: config.pool.acquire,
            idle: config.pool.idle,
        },
    }
);
// connection.sync();
// (async () => {
//     try {
//         await sequelize.authenticate();
//         console.log("connection to db successfull");
//     } catch (error) {
//         console.log(error);
//     }
// })();
module.exports = { connection };
