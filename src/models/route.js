const Sequelize = require("sequelize");
const Bus = require("./bus");
const City = require("./city");
const { connection } = require("./index");

const Route = connection.define("routes", {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false,
    },
    departureTime: {
        type: Sequelize.TIME,
        allowNull: false,
    },
    arrivalTime: {
        type: Sequelize.TIME,
        allowNull: false,
    },
    departureDate: {
        type: Sequelize.DATEONLY(),
        allowNull: false,
    },
    arrivalDate: {
        type: Sequelize.DATEONLY(),
        allowNull: false,
    },
    fare: {
        type: Sequelize.INTEGER(),
        allowNull: false,
    },
});

Bus.hasMany(Route, {
    foreignKey: "busID",
});
City.hasMany(Route, {
    foreignKey: "sourceCityID",
});
City.hasMany(Route, {
    foreignKey: "destinationCityID",
});

module.exports = Route;
