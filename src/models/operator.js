const Sequelize = require("sequelize");
const { connection } = require("./index");

const Operator = connection.define("operators", {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false,
    },
    operator: {
        type: Sequelize.STRING,
        allowNull: false,
    },
});

module.exports = Operator;
