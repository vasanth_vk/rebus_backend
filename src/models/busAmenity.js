const Sequelize = require("sequelize");
const Amenity = require("./amenity");
const { connection } = require("./index");
const Bus = require("./bus");

const BusAmenity = connection.define("busAmenity", {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false,
    },
});

Bus.hasMany(BusAmenity, {
    foreignKey: "busID",
});
Amenity.hasMany(BusAmenity, {
    foreignKey: "amenitiesID",
});

module.exports = BusAmenity;
