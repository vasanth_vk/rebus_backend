const Sequelize = require("sequelize");
const { connection } = require("./index");

const Amenity = connection.define("amenities", {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false,
    },
    amenity: {
        type: Sequelize.STRING,
        allowNull: false,
    },
});

module.exports = Amenity;
