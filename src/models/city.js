const Sequelize = require("sequelize");
const { connection } = require("./index");

const City = connection.define("cities", {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false,
    },
    city: {
        type: Sequelize.STRING,
        allowNull: false,
    },
});

module.exports = City;
