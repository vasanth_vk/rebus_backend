const Sequelize = require("sequelize");
const { connection } = require("./index");
const Route = require("./route");

const Seat = connection.define("seats", {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false,
    },
    seat: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    seatBooked: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
    },
});
Route.hasMany(Seat, {
    foreignKey: "routeID",
});

module.exports = Seat;
